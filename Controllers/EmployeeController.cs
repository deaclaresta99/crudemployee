﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using crudemployee.DataContext;
using crudemployee.Models;

namespace crudemployee.Controllers
{
    public class EmployeeController : Controller
    {
        private ApplicationDBContext db = new ApplicationDBContext();

        // GET: Employee
        public ActionResult Index()
        {
            return View(db.EmployeeObject.ToList());
        }

        // GET: Employee/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeClass employeeClass = db.EmployeeObject.Find(id);
            if (employeeClass == null)
            {
                return HttpNotFound();
            }
            return View(employeeClass);
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmployeeId,EmployeeName,Email,Salary")] EmployeeClass employeeClass)
        {
            if (ModelState.IsValid)
            {
                db.EmployeeObject.Add(employeeClass);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(employeeClass);
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeClass employeeClass = db.EmployeeObject.Find(id);
            if (employeeClass == null)
            {
                return HttpNotFound();
            }
            return View(employeeClass);
        }

        // POST: Employee/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmployeeId,EmployeeName,Email,Salary")] EmployeeClass employeeClass)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employeeClass).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employeeClass);
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeClass employeeClass = db.EmployeeObject.Find(id);
            if (employeeClass == null)
            {
                return HttpNotFound();
            }
            return View(employeeClass);
        }

        // POST: Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EmployeeClass employeeClass = db.EmployeeObject.Find(id);
            db.EmployeeObject.Remove(employeeClass);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
