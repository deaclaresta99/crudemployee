﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using crudemployee.Models;

namespace crudemployee.DataContext {
    public class ApplicationDBContext :DbContext {
        public ApplicationDBContext():base(nameOrConnectionString: "Myconnection") {

        }
        public virtual DbSet<EmployeeClass> EmployeeObject { get; set; }
    }
}