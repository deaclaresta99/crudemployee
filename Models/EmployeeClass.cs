﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace crudemployee.Models {
    [Table("employee", Schema="public")]
    public class EmployeeClass {
        [Key]   
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }

        public string Email { get; set; }
        public int Salary { get; set; }
    }
}